//
//  ViewController.swift
//  LabelScaleDemoTest
//
//  Created by Andrew Benson on 9/21/18.
//  Copyright © 2018 Nuclear Cyborg Corp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label2: UILabel!

    func updateLabels(_ s: String) {
        // top label updates automatically
        label.text = s


        // manually update bottom label
        var size = 222 as CGFloat
        repeat {
            let textSize = s.size(withAttributes:[.font: UIFont.systemFont(ofSize:size)])
            if textSize.width <= label2.frame.width {
                break
            }
            size -= 0.10

        } while (size > 4)
        label2.font = UIFont.systemFont(ofSize: size)
        label2.text = s
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        textField.delegate = self

        label.font = UIFont.systemFont(ofSize: 222)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.0001

        updateLabels("Hello!")
    }
}


extension ViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""

        updateLabels(updatedString)
        return true
    }
}
